#1.MYSQL的测试库和测试表
create database if not exists yunque;

CREATE TABLE  IF NOT EXISTS yunque.`source` (
  `id` varchar(100) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `address` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `student_id_IDX` (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
INSERT INTO yunque.source (id,name,address) VALUES
('1','张三','北京'),
('2','李四','上海');

CREATE TABLE  IF NOT EXISTS yunque.`sink` (
  `id` varchar(100) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `address` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `student_id_IDX` (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


