package www.larkmidtable.com.queue;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;
import www.larkmidtable.com.element.Record;
import www.larkmidtable.com.transformer.TransformerExecution;
import www.larkmidtable.com.transformer.TransformerParameterInfo;

import java.util.List;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * @Author: Gooch
 */
public class DefaultQueueV2 extends ConcurrentLinkedQueue<Record> {
    private static final Logger logger = LoggerFactory.getLogger(DefaultQueueV2.class);
    private static List<TransformerExecution> transformerExecutionList = null;

    public DefaultQueueV2(List<TransformerExecution> transformerExecutionList_) {
        transformerExecutionList = transformerExecutionList_;
    }

    @Override
    public boolean add(Record record) {
        logger.debug("添加record：{}", record);
        Record newRecord = record;
        if (!CollectionUtils.isEmpty(transformerExecutionList)) {
            for (TransformerExecution transformerExecution : transformerExecutionList) {
                TransformerParameterInfo parameter = transformerExecution.getTransformerInfo().getParameter();
                newRecord = transformerExecution.getTransformer().evaluate(newRecord, parameter);
            }
        }
        super.add(newRecord);
        return true;
    }

    @Override
    public Record poll() {
        return super.poll();
    }
}
