package www.larkmidtable.com.reader;

import www.larkmidtable.com.bean.ConfigBean;
import www.larkmidtable.com.constant.ReaderPluginEnum;
import www.larkmidtable.com.element.Record;
import www.larkmidtable.com.exception.YunQueException;
import www.larkmidtable.com.log.LogRecord;
import www.larkmidtable.com.model.TaskParams;
import www.larkmidtable.com.model.TaskResult;

import java.util.List;
import java.util.Queue;
import java.util.function.Supplier;

/**
 *
 *
 * @Date: 2022/11/14 11:03
 * @Description:
 **/
public abstract class Reader {

	protected Queue<Record> queue;

	protected ConfigBean configBean;

	protected LogRecord logRecord;

	public ConfigBean getConfigBean() {
		return configBean;
	}

	public void setConfigBean(ConfigBean configBean) {
		this.configBean = configBean;
	}

	// 初始化操作
	@Deprecated
	public void open() {};

	// 初始化操作
	public abstract void init(int pos, int toal);

	public abstract void read(Queue<Record> queue);

	// 读取数据操作
	@Deprecated
	public Queue<List<String>> startRead(String[] inputSplits) { return null;};

	@Deprecated
	public Queue<List<String>> startRead(String inputSplit) { return null;};
	// SQL的切片划分
	public abstract String[] createInputSplits(int count,int bcount);

	// 关闭操作
	@Deprecated
	public void close() {};
	// 关闭操作
	public abstract void stop();


	public static Reader getReaderPlugin(String name, ConfigBean readerConfigBean) {
		try {
			Reader reader = (Reader) Class.forName(ReaderPluginEnum.getByName(readerConfigBean.getPlugin()).getClassPath()).newInstance();
			reader.setConfigBean(readerConfigBean);
			return reader;
		} catch (Exception e) {
			throw new YunQueException("文件获取不到", e);
		}

	}

	public abstract class ReadTask<P extends TaskParams, R extends TaskResult> implements Supplier<TaskResult> {

		protected P taskParams;

		public ReadTask() {
		}

		public abstract void preProcess();
		public abstract R doProcess();
		public abstract R postProcess(R processResult);
		@Override
		public R get() {
			preProcess();
			return postProcess(doProcess());
		}
	}
}
