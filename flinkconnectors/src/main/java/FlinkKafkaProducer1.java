import lombok.Data;
import org.apache.flink.api.common.serialization.SimpleStringSchema;
import org.apache.flink.api.common.typeinfo.TypeInformation;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaProducer;
import org.apache.flink.streaming.util.serialization.KeyedDeserializationSchema;
import org.apache.kafka.clients.producer.ProducerConfig;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Properties;


public class FlinkKafkaProducer1 {

	public static void main(String[] args) throws Exception {
		// 0 初始化 flink 环境
		StreamExecutionEnvironment env =
				StreamExecutionEnvironment.getExecutionEnvironment();
		env.setParallelism(1);
		// 1 读取集合中数据
		ArrayList<String> wordsList = new ArrayList<>();
		wordsList.add("hello");
		wordsList.add("world");
		wordsList.add("offset 1");
		wordsList.add("offset 2");
		wordsList.add("offset 3");

		DataStream<String> stream = env.fromCollection(wordsList);
		// 2 kafka 生产者配置信息
		Properties properties = new Properties();
//		properties.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "hadoop102:9092");
		properties.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "100.100.100.1:9092");
		// 3 创建 kafka 生产者
		FlinkKafkaProducer<String> kafkaProducer = new FlinkKafkaProducer<>(
				"first1",
				new SimpleStringSchema(),
				properties
		);
		// 4 生产者和 flink 流关联
		stream.addSink(kafkaProducer);
		// 5 执行
		env.execute();
	}
}
