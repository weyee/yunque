package com.larkmidtable.yunque.rpc;

import org.apache.commons.cli.ParseException;

public interface ServerService {
	String executeSQL(Object key,int pos,int total) throws ParseException;
}
